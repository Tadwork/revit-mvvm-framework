﻿using System;
using System.Diagnostics;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using RevitMVVM.Dialogs;
using RMVVM.Framework;
using RMVVM.Model;

namespace RMVVM
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class Show : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            var vModel = new UserViewModel();
            vModel.Show();

            return Result.Succeeded;
        }
    }


    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class ShowDialog : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,
          ElementSet elements)
        {
            //var user = new User { FirstName = "Nathan", LastName = "Wecksler" };
            var vModel = new UserViewModel();
            vModel.ShowDialog();

            return Result.Succeeded;
        }
    }
}
