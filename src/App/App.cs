﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.UI;
using RMVVM.Framework;

namespace RMVVM.App
{
   public class App : IExternalApplication
    {
        private ControlledApplication _app;
        private UIControlledApplication _uiApp;


        #region IExternalApplication Members

        public Result OnStartup(UIControlledApplication application)
        {
            _app = application.ControlledApplication;
            _uiApp = application;

            UI.UIApp = application;
            AddMenu();


            IoCBuilder.CollectViewAndViewModelMappings();

            return Result.Succeeded;
        }


        public Result OnShutdown(UIControlledApplication application)
        {

            return Result.Succeeded;
        }

        #endregion


       


        private void AddMenu()
        {
            _uiApp.CreateRibbonTab(UI.AppName);

            if (!UI.IsPanelTitleUsed(UI.AppName, UI.AppName))
            {
                RibbonPanel toolPanel = _uiApp.CreateRibbonPanel(UI.AppName, UI.AppName);

                UI.AddPushButton(toolPanel, "Show", "Show");
                UI.AddPushButton(toolPanel, "ShowDialog", "ShowDialog");
            }


         

        }


    }



  
    }

