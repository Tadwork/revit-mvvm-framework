﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using RevitMVVM.Dialogs;
using RMVVM.Framework;

namespace RMVVM
{
    public class ViewModelBase 
    {

        public void Show()
        {
            Process process = Process.GetCurrentProcess();
            IntPtr h = process.MainWindowHandle;

            var dlg = new UserDialog();
            dlg.Show(new UI.JtWindowHandle(h));
        }



        public void ShowDialog()
        {
            var dlg = new UserDialog();
            dlg.ShowDialog();
        }
    }
}