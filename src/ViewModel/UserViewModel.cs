﻿
using System;
using System.Diagnostics;
using System.Windows.Input;
using RevitMVVM.Dialogs;
using RMVVM.Framework;
using RMVVM.Model;

namespace RMVVM
{
    public sealed class UserViewModel : ViewModelBase
    {

        public User User { get; set; }

        public UserViewModel()
        {
            User = new User { FirstName = "John", LastName = "Doe"};
        }


    }
}
