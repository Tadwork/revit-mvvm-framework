namespace RMVVM.Framework
{
    using Microsoft.Practices.Unity;

    public static class ServiceLocator
    {
        public static readonly IUnityContainer IoC = new UnityContainer();

        static ServiceLocator()
        {
            IoC.RegisterInstance(IoC, new ContainerControlledLifetimeManager());
        }   
    }
}