﻿using RMVVM.Framework;
using System.Windows;
using System.Windows.Interactivity;


namespace Framework.Behaviors
{
    public class AutoWireUpViewModelBehavior : Behavior<UIElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            var view = (FrameworkElement)this.AssociatedObject;
            var viewModelName = string.Format("{0}Model", view.GetType().FullName);
            var viewModel = ServiceLocator.IoC.Resolve<object>(viewModelName);
            view.DataContext = viewModel;
        }
    }
}