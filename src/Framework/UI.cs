﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using Autodesk.Revit.UI;

namespace RMVVM.Framework
{
    public static class UI
    {
        private static readonly string ExecutingAssemblyPath = Assembly.GetExecutingAssembly().Location;
        public static UIControlledApplication UIApp { get; set; }
        public static string AppPath { get; set; }
        public static readonly string AppName = "RMVVM";


        public static bool IsPanelTitleUsed(string tab, string panelTitle)
        {
            List<RibbonPanel> loadedPluginPanels = UIApp.GetRibbonPanels(tab);

            foreach (RibbonPanel p in loadedPluginPanels)
            {
                if (p.Name == panelTitle)
                {
                    return true;
                }
            }
            return false;
        }



        public static void AddPushButton(RibbonPanel panel, string name, string command)
        {

            if (string.IsNullOrEmpty(command))
            {
                command = name;
            }

            var pushButton = panel.AddItem(new PushButtonData(name,
        name, ExecutingAssemblyPath, UI.AppName + "." + command)) as PushButton;
        }



        public class JtWindowHandle : IWin32Window
        {
            readonly IntPtr _hwnd;

            public JtWindowHandle(IntPtr h)
            {
                Debug.Assert(IntPtr.Zero != h,
                  "expected non-null window handle");

                _hwnd = h;
            }

            public IntPtr Handle
            {
                get
                {
                    return _hwnd;
                }
            }
        }

    }
    
}