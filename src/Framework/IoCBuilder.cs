namespace RMVVM.Framework
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;

    public static class IoCBuilder
    {
        public static void CollectViewAndViewModelMappings()
        {
            foreach (var type in Assembly.GetCallingAssembly().GetTypes())
            {
                var typeIsUserControl = type.BaseType == typeof(UserControl);
                if (typeIsUserControl)
                {
                    var typeIsView = type.Name.EndsWith("View", StringComparison.InvariantCultureIgnoreCase);
                    if (typeIsView)
                    {
                        ServiceLocator.IoC.RegisterType(typeof(FrameworkElement), type, type.FullName);
                    }
                }
                else
                {
                    var typeIsViewModel = type.Name.EndsWith("ViewModel", StringComparison.InvariantCultureIgnoreCase);
                    if (typeIsViewModel)
                    {
                        ServiceLocator.IoC.RegisterType(typeof(object), type, type.FullName);
                    }

                }
            }
        }  
     


    }
}